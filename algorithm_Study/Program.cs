﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algorithm_Study
{
    class Program
    {
        static void Main(string[] args)
        {
            string selectNumber;
            // Binary Search
            int[] dataArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
            // Selection Sort
            int[] dataArr2 = { 5, 10, 15, 2, 7, 8, 9, 11, 3, 1 };

            BinarySearchAlgorithm bs = new BinarySearchAlgorithm();
            SelectionSortAlgorithm ss = new SelectionSortAlgorithm();

            Output output = new Output();

            while (true)
            {
                output.Output_Main();
                Console.WriteLine("=================================");
                output.Output_Command();
                Console.Write(">>");

                selectNumber = Console.ReadLine();
                
                switch (Int32.Parse(selectNumber))
                {
                    case 0:
                        return;
                    case 1:
                        int ret = bs.BinarySearch(dataArr, dataArr.Length, 17);

                        if (ret != -1)
                        {
                            Console.WriteLine("찾는 데이터는 {0}번째에 있습니다.", ret);
                        }
                        else
                        {
                            Console.WriteLine("데이터를 찾을수 없습니다.");
                        }
                        break;
                    case 2:
                        Console.Write("Before : ");
                        foreach (int i in dataArr2)
                        {
                            Console.Write(i + " ");
                        }
                        Console.WriteLine();
                                                
                        ss.SelectionSort(dataArr2);
                        break;

                }
            } // End of While
        }
    }
}
