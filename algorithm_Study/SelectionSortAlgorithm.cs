﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algorithm_Study
{
    class SelectionSortAlgorithm
    {
        // SelectionSort(선택 정렬)
        // 1. 주어진 리스트 중에서 최소값을 찾는다.
        // 2. 그 값을 맨 앞에 위치한 값과 교체한다.
        // 3. 매 처음 위치를 뺀 나머지 리스트를 같은 방법으로 교체한다.

        public SelectionSortAlgorithm()
        {

        }

        public void SelectionSort(int[] list)
        {
            int Min, temp;

            for (int i = 0; i < list.Length - 1; i++)
            {
                Min = 1;
                for (int j = i + 1; j < list.Length; j++)
                {
                    if (list[j] < list[Min])
                    {
                        Min = j;
                    }

                    temp = list[Min];
                    list[Min] = list[i];
                    list[i] = temp;
                }
            }
        }
    }
}
