﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algorithm_Study
{
    class BinarySearchAlgorithm
    {
        // Binary Search(이진 탐색)
        // 한번 비교를 거칠때 탐색 범위가 (1/2)으로 줄어들기 때문에 이진 탐색(Binary Search)라고 붙여짐.
        // 정렬된 배열을 전제로 함.
        // ex) 70억명 중 특정한 정보를 탐색하려 할때. 순차 탐색은 평균 35억번 이진 탐색은 최대 33번 비교로 데이터를 찾음.

        public BinarySearchAlgorithm()
        {

        }
        /*
         * 탐색 과정
         * 1. 데이터 집합의 중앙 요소를 선택
         * 2. 중앙 요소 값과 찾을려는 값을 비교
         * 2-1. 찾으려는 값이 중앙 요소 값보다 작으면 중앙 요소 왼편에서 중앙 요소를 다시 선택.
         * 2-2. 찾으려는 값이 중앙 요소 값보다 크면 중앙 요소 오른편에서 중앙 요소를 다시 선택.
         * 3. 찾는 값이 나올때가지 반복 하며, 일치하면 탐색 종료.
         */

        public int BinarySearch(int[] dataArr, int size, int findData)
        {
            int low = 0;
            int high = size - 1;
            int mid;

            while (low <= high)
            {
                mid = (low + high) / 2;

                if (dataArr[mid] > findData)
                {
                    high = mid - 1;
                }
                else if (dataArr[mid] < findData)
                {
                    low = mid + 1;
                }
                else
                {
                    return mid;
                }
            }

            return -1;
        }

    }
}
